# Guide installation


## Prérequis : Wordpress Version 5.9.2, mysql...


### 1) Installation des fichiers

- Telecharger et dezipper wordpress

- Cloner le répo

- Remplacer le dossier wp-content de wordpress votre par celui cloné

## 2) Initialisation du wordpress

- Créer une table mysql Commerce puis importer le fichier dump Commerce.sql à l'intérieur

- Initialiser wordpress (localhost) en utilisant la table Commerce

- Acceder a votre wp-admin

- Menu outil -> importer -> wordpress
- Installer le plugin puis importer nfdco.WordPress.2022-03-31.xml

- Aller dans extensions et activer les plugins de woocommerce et stripe

- Aller dans le menu themes
- Utiliser "childtheme"

