<?php
/**
 * The template for displaying all single posts.
 *
 * @package storefront
 */

get_header(); 
do_action( 'storefront_sidebar' );?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

        <h1 class="indextitle">Une gamme de produits</h1>

        <div class="divider"></div>

        <h2>Unique</h2>
        <p>Trouvable nulle part ailleurs et au style bien propre, vous pouvez êtes certains
            de ne pas retrouver une décoration similaire ailleurs !
        </p>
        <h2>Exclusif</h2>
        <p>Nos produits sont en stock limité, ainsi donc ils sont non seulement très rare,
            mais de plus vous n'aurez donc pas d'autres chance de pouvoir
            vous les procurer, alors qu'attendez vous !
        </p>
        <h2>Raffiné</h2>
        <p>Une ésthetique moderne, delicate, émouvante et personnelle permettant de sublimer 
            votre interieur. C'est la cerise sur le gateau qu'il vous manquais !
        </p>



		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
