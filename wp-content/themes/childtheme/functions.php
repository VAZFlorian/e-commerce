<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

// END ENQUEUE PARENT ACTION


add_action( 'woocommerce_before_shop_loop_item_title', 'wpm_display_sold_out_loop_woocommerce' );// On l'affiche sur la page boutique
add_action( 'woocommerce_single_product_summary', 'wpm_display_sold_out_loop_woocommerce' );// On l'affiche sur la page du produit seul
 
function wpm_display_sold_out_loop_woocommerce() {
    global $product;
     //Si le produit est en rupture de stock, on affiche :
    if ( !$product->is_in_stock() ) {
        echo '<span class="soldout">' . __( 'Indisponible', 'woocommerce' ) . '</span>';
    }
}